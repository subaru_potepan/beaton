// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require activestorage
//= require turbolinks
//= require_tree .


$(function () {
    $("#beatone").fadeIn(2000);
    $("#main-message").fadeIn(6000);
    
    $(".icon").click(function () {
       $("#overlay").fadeIn();
       $("#menu-box").fadeIn();
    });

    $(".close").click(function () {
        $("#overlay").fadeOut();
        $("#menu-box").fadeOut();
    })
});